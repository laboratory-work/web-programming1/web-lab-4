const merge = require('webpack-merge');
const WebCommon = require('./webpack.common.js');

module.exports = merge(WebCommon, {
    mode: 'development',
    devtool: 'source-map',
    devServer: {
        contentBase: './dist',
        compress: true,
        port: 8000,
        allowedHosts: [
            'localhost:9000',
        ],
        stats: 'normal',
        clientLogLevel: 'warning'
    },
});