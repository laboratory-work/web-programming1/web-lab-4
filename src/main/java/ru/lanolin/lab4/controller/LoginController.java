package ru.lanolin.lab4.controller;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.lanolin.lab4.config.auth.SecurityService;
import ru.lanolin.lab4.domain.User;
import ru.lanolin.lab4.domain.Views;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class LoginController {

	private final SecurityService securityService;

	@Autowired
	public LoginController(SecurityService securityService) {
		this.securityService = securityService;
	}

	@PostMapping("/api/login")
	@JsonView(Views.IdName.class)
	public User login(HttpServletRequest request, @RequestBody User user){
		securityService.loginUser(request, user);
		return user;
	}

	@SneakyThrows
	@PostMapping("/api/logout")
	@JsonView(Views.IdName.class)
	public void logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
//		return "redirect:/";
	}

}
