package ru.lanolin.lab4.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.lanolin.lab4.domain.Point;
import ru.lanolin.lab4.domain.Views;
import ru.lanolin.lab4.dto.CheckAreaToHitService;
import ru.lanolin.lab4.dto.PointDTO;
import ru.lanolin.lab4.exception.NotFoundException;
import ru.lanolin.lab4.repo.PointRepo;
import ru.lanolin.lab4.repo.UserRepo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/points")
public class PointController {

	private final PointRepo pointRepo;
	private final UserRepo userRepo;
	private final CheckAreaToHitService checkAreaToHitService;

	@Autowired
	public PointController(PointRepo pointRepo, UserRepo userRepo, CheckAreaToHitService checkAreaToHitService) {
		this.pointRepo = pointRepo;
		this.userRepo = userRepo;
		this.checkAreaToHitService = checkAreaToHitService;
	}

	private PointDTO convertPointToPointDTO(Point p){
		return new PointDTO(p.getId(),p.getX(), p.getY(), p.getR(), getHit(p), p.getTime(), p.getOwner());
	}
	private PointDTO convertPointToPointDTO(Point p, String r){
		return new PointDTO(p.getId(),p.getX(), p.getY(), r, getHit(p, r), p.getTime(), p.getOwner());
	}
	private Boolean getHit(Point p){ return checkAreaToHitService.hit(p.getX(), p.getY(), p.getR()); }
	private Boolean getHit(Point p, String r){ return checkAreaToHitService.hit(p.getX(), p.getY(), r); }

	@GetMapping
	@JsonView(Views.IdName.class)
	public List<PointDTO> get(){
		return pointRepo.findAll().stream().map(this::convertPointToPointDTO).collect(Collectors.toList());
	}

	@GetMapping("radius/{radius}")
	@JsonView(Views.IdName.class)
	public List<PointDTO> getOne(@PathVariable(name = "radius") String radius){
		return pointRepo.findAll().stream().map(val -> convertPointToPointDTO(val, radius)).collect(Collectors.toList());
	}

	@GetMapping("{id}")
	@JsonView(Views.IdName.class)
	public PointDTO getOne(@PathVariable(name = "id") Point point){
		return convertPointToPointDTO(pointRepo.getOne(point.getId()));
	}

	@PostMapping
	@JsonView(Views.Id.class)
	public PointDTO create(@AuthenticationPrincipal User user, @RequestBody Point point){
		point.setTime(LocalDateTime.now());
		point.setR(point.getR().replace(',', '.'));
		point.setOwner(userRepo.findByLogin(user.getUsername()).orElseThrow(NotFoundException::new));
		return convertPointToPointDTO(pointRepo.save(point));
	}

	@PutMapping("{id}")
	@JsonView(Views.IdName.class)
	public PointDTO update(
			@PathVariable(name = "id") Point pointFromDB,
			@RequestBody Point point
	){
		BeanUtils.copyProperties(point, pointFromDB, "id", "owner");
		pointFromDB.setR(point.getR().replace(',', '.'));
		pointFromDB.setTime(LocalDateTime.now());
		return convertPointToPointDTO(pointRepo.save(pointFromDB));
	}

	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") Point point){
		pointRepo.delete(point);
	}
}
