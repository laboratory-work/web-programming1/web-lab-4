package ru.lanolin.lab4.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.lanolin.lab4.domain.Views;
import ru.lanolin.lab4.exception.NotFoundException;
import ru.lanolin.lab4.repo.PointRepo;
import ru.lanolin.lab4.repo.UserRepo;

import java.util.HashMap;

@Controller
@RequestMapping("/")
public class MainController {

	private final UserRepo userRepo;
	private final ObjectWriter profileWriter;

	@Value("${spring.profiles.active:production}")
	private String profile;

	@Autowired
	public MainController(UserRepo userRepo, ObjectMapper mapper, PointRepo pointRepo) {
		this.userRepo = userRepo;

		ObjectMapper objectMapper = mapper
				.setConfig(mapper.getSerializationConfig());

		this.profileWriter = objectMapper.writerWithView(Views.IdName.class);
	}

	@SneakyThrows
	@GetMapping
	@JsonView(Views.IdName.class)
	@DateTimeFormat(pattern = "YYYY-MM-dd HH:mm:ss")
	public String main(@AuthenticationPrincipal User user, Model model){
		HashMap<String, Object> data = new HashMap<>();

		if(user != null){
			ru.lanolin.lab4.domain.User userFromDB = userRepo.findByLogin(user.getUsername()).orElseThrow(NotFoundException::new);
			String serProfile = profileWriter.writeValueAsString(userFromDB);
			data.put("profile", serProfile);
		}else{
			data.put("profile", "null");
		}

		data.put("title", "Лабораторная 4");
//		data.put("isDevMode", false);
		data.put("isDevMode", "develop".equalsIgnoreCase(profile));
		model.addAllAttributes(data);
		return "index";
	}

}
