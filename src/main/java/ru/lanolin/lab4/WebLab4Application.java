package ru.lanolin.lab4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class WebLab4Application {

	public static void main(String[] args) {
		SpringApplication.run(WebLab4Application.class, args);
	}

}
