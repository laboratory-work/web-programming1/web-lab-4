package ru.lanolin.lab4.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.RepositoryDefinition;
import ru.lanolin.lab4.domain.User;

import java.util.Optional;

@RepositoryDefinition(domainClass = User.class, idClass = Long.class)
public interface UserRepo extends JpaRepository<User, Long> {

	Optional<User> findByLogin(String name);
	boolean existsUserByLogin(String login);
}
