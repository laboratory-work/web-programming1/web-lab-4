package ru.lanolin.lab4.config.auth;

import ru.lanolin.lab4.domain.User;

import javax.servlet.http.HttpServletRequest;

public interface SecurityService {

	void loginUser(HttpServletRequest request, User logingUser);

}
