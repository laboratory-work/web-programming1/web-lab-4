package ru.lanolin.lab4.config.auth;

import ru.lanolin.lab4.domain.User;

public interface UserService {
	void save(User user);
	boolean existUser(String username);
	void overrideId(User usr);
}
