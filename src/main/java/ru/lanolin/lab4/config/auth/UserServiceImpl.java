package ru.lanolin.lab4.config.auth;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.lanolin.lab4.domain.User;
import ru.lanolin.lab4.exception.NotFoundException;
import ru.lanolin.lab4.repo.UserRepo;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepo userRepo;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public UserServiceImpl(UserRepo userRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userRepo = userRepo;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public void save(User user) {
		User u = new User();
		BeanUtils.copyProperties(user, u, "id");
		u.setPassword(bCryptPasswordEncoder.encode(u.getPassword()));
		userRepo.save(u);
		user.setId(u.getId());
	}

	@Override
	public void overrideId(User usr){
		User u = userRepo.findByLogin(usr.getLogin()).orElseThrow(NotFoundException::new);
		usr.setId(u.getId());
	}

	@Override
	public boolean existUser(String username) {
		return userRepo.existsUserByLogin(username);
	}
}
