package ru.lanolin.lab4.dto;

public interface CheckAreaToHitService {
	Boolean hit(String x, String y, String r);
}
