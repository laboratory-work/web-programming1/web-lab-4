package ru.lanolin.lab4.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.lanolin.lab4.domain.User;
import ru.lanolin.lab4.domain.Views;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@EqualsAndHashCode(exclude = { "hit", "owner" })
@ToString(of = {"id", "x", "y", "r", "time"})
@JsonView(Views.IdName.class)
public class PointDTO implements Serializable {

	@JsonView(Views.Id.class)
	private Long id;
	@JsonView(Views.IdName.class)
	private String x;
	@JsonView(Views.IdName.class)
	private String y;
	@JsonView(Views.IdName.class)
	private String r;
	@JsonView(Views.IdName.class)
	private Boolean hit;

	@JsonView(Views.IdName.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime time;
	@JsonView(Views.IdName.class)
	private User owner;

}
