package ru.lanolin.lab4.dto;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CheckAreaToSomeFunctionImpl implements CheckAreaToHitService {

	@Override
	public Boolean hit(String xS, String yS, String rS) {
		//TODO Проверить наличие отрицательного радиуса

		rS = rS.replace(',', '.');

		BigDecimal x = new BigDecimal(xS);
		BigDecimal y = new BigDecimal(yS);
		BigDecimal r = new BigDecimal(rS);

		if(x.compareTo(BigDecimal.valueOf(-3)) < 0 || x.compareTo(BigDecimal.valueOf(3)) > 0) return null;
		if(y.compareTo(BigDecimal.valueOf(-5)) < 0 || y.compareTo(BigDecimal.valueOf(3)) > 0) return null;
		if(r.compareTo(BigDecimal.valueOf(0)) < 0 || r.compareTo(BigDecimal.valueOf(3)) > 0) return null;

		int compareX = x.compareTo(BigDecimal.ZERO);
		int compareY = y.compareTo(BigDecimal.ZERO);

		if (compareX >= 0 && compareY >= 0) { //1 четверть
			return y.compareTo(x.multiply(BigDecimal.valueOf(-2)).add(r)) <= 0;
		} else if (compareX <= 0 && compareY >= 0) { //2 четверть
			return x.pow(2).add(y.pow(2)).compareTo(r.pow(2)) <= 0;
		} else if (compareX <= 0) {  //3 четверть
			return x.compareTo(r.negate()) >= 0 && y.compareTo(r.negate()) >= 0;
		} else {  //4 четверть
			return false;
		}
	}

}
