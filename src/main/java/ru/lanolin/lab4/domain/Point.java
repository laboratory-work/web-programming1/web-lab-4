package ru.lanolin.lab4.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "points", schema = "s264472")
public class Point implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String x;
	private String y;
	private String r;
//	private Boolean hit;
	private LocalDateTime time;

	@OneToOne
	@JoinColumn(name = "owner_id")
	private User owner;
}

