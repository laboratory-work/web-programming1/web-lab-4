package ru.lanolin.lab4.domain;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "users", schema = "s264472")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Id.class)
	private Long id;

	@JsonView(Views.IdName.class)
	private String login;

	@JsonView(Views.FullProfile.class)
	private String password;

}
