import BigNumber from "bignumber.js";

const fpRegex = /^[\x00-\x20]*[+-]?(NaN|Infinity|((((\d+)(\.)?((\d+)?)([eE][+-]?(\d+))?)|(\.((\d+))([eE][+-]?(\d+))?)[pP][+-]?(\d+)))[fFdD]?)[\x00-\x20]*$/;

export function isNumber(checking = "") {
    let result = checking.match(fpRegex);
    return !!result;
}

export function validateRange(check, min= 0, max= 1) {
    if(!isNumber(check)) return false;

    const minB = BigNumber(min);
    const maxB = BigNumber(max);
    const checkB = BigNumber(check);

    return minB.isLessThan(checkB) && checkB.isLessThan(maxB);
}
