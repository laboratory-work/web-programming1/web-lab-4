import Vue from 'vue'
import VueRouter from 'vue-router'
import Container from "../views/app/Container.vue";
import Login from "../views/login/Login.vue";

Vue.use(VueRouter);

const routes = [
    { path: '/', component: Container },
    { path: '/login', component: Login },
    { path: '*', component: Container}
];

export default new VueRouter({
    mode: 'history',
    routes
})