import Vue from 'vue';
import Vuex from 'vuex';
import router from './router'
import Graphics from "../api/graphics";
import pointApi from "../api/points";
import {isNumber, validateRange} from "./utils";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        profile: profile,
        points: [],
        radius: 1,
        graphics: '',
        xRules: [
            v => !!v || 'X is required',
            v => (v && v.length <= 15) || 'X must be less than 15 characters',
            v => (v && isNumber(v)) || 'X must be a decimal number',
            v => (v && validateRange(v, -3, 3)) || 'X must be in range (-3..3)',
        ],
        yRules: [
            v => !!v || 'Y is required',
            v => (v && v.length <= 15) || 'Y must be less than 15 characters',
            v => (v && isNumber(v)) || 'Y must be a decimal number',
            v => (v && validateRange(v, -5, 3)) || 'Y must be in range (-5..3)',
        ],
        rRules: [
            v => !!v || 'R is required',
            v => (v && v.length <= 15) || 'R must be less than 15 characters',
            v => (v && isNumber(v)) || 'R must be a decimal number',
            v => (v && validateRange(v, 0, 3)) || 'R must be in range (0..3)',
        ],
    },
    getters: {
        isLoadUser: state => state.profile !== null,
        getNickName: state => state.profile.login,
        getRadius: state => state.radius,
        getXRules: state => state.xRules,
        getYRules: state => state.yRules,
        getRRules: state => state.rRules,
        isMe: state => id => state.profile.id === id,
        getAllPoints: state => state.points,
        getSortedPoints: (state, getters) => getters.getAllPoints.sort( (a, b) => a.id - b.id ),
    },
    mutations: {
        updateRadiusMutation(state, radius){
            state.radius = radius;
        },
        saveUserCreditMutation(state, user){
            state.profile = user;
        },
        logoutMutation(state){
            state.profile = null;
        },
        createGraphics(state){
            state.graphics = new Graphics("#canvas_1", "#canvas_2");
        },
        addPointPageMutation(state, points){
            const targetPoints = state.points.concat(points)
                .reduce((res, val) => {
                    res[val.id] = val;
                    return res
                }, {});

            state.points = Object.values(targetPoints);
        },
        updateRadiusPointMutation(state, nPoint){
            state.graphics.drawPoint(nPoint);
        }
    },
    actions: {
        async loginUserAction({ commit }, {payload, errorHandler}){
            await Vue.resource("/api/login").save(payload).then(
                res => { res.json().then(json => commit('saveUserCreditMutation', json));  router.replace("/") },
                err => { errorHandler(); }
            );
        },
        async logoutUserAction({ commit }){
            await Vue.resource("/api/logout").save().then(
                res => { commit('logoutMutation'); router.replace("/login") }
            );
        },
        async loadPointsAction({ dispatch, commit, state }) {
            // if(state.profile) {
                const result = await pointApi.get();
                const json = await result.json();

                commit("addPointPageMutation", json);
                await dispatch("recountPointsRadius", state.radius);
            // }
        },
        async recountPointsRadius({ commit, state }, radius){
            // if(state.profile) {
                const result = await pointApi.radius(radius);
                const json = await result.json();
                commit("updateRadiusPointMutation", json);
            // }
        },
        async addPointAction({ dispatch, commit, state}, point){
            // if(state.profile) {
                await pointApi.save(point);
                await dispatch("loadPointsAction");
                await dispatch("recountPointsRadius", state.radius);
            // }
        },
        async updatePointAction({ dispatch, commit, state }, point){
            // if(state.profile) {
                const result = await pointApi.update(point);
                await dispatch("loadPointsAction");
            // }
        },
    }
});