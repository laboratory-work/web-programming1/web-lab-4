import Vue from 'vue'

const pointApi = Vue.resource("/api/points{/id}");

export default {
    get: id => pointApi.get({id}),
    update: point => pointApi.update({id: point.id}, point),
    remove: id => pointApi.remove({id}),
    radius: radius => Vue.http.get('/api/points/radius{/radius}', {params: { radius }}),
    save: point => pointApi.save({}, point)
}