import Vue from 'vue';

import vuetify from './utils/vuetify';

import '@babel/polyfill'

import JCanvas from "jcanvas";
JCanvas($, window);

import './api/resource';

import router from './utils/router';
import store from './utils/store';
import App from "./views/App.vue";

new Vue({
    el: "#app",
    router,
    store,
    vuetify,
    render: a => a(App)
});